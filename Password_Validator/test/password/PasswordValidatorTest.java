package password;

/**
 * 
 * @author Jonathan Sriskandarajah 991531114
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLength() {
		boolean result = PasswordValidator.isValidLength("Jonathan1234");
		assertTrue("Invalid Password Length", result);
	}

	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("");
		assertFalse("Invalid Password Length", result);
	}

	@Test
	public void testIsValidLengthBoundryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid Password Length", result);
	}

	@Test
	public void testIsValidLengthBoundryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid Password Length", result);
	}

	@Test
	public void testHasValidDigits() {
		boolean result = PasswordValidator.hasValidDigits("Jonathan22");
		assertTrue("Invalid Password must contain at least 2 digits", result);
	}

	@Test
	public void testHasValidDigitsException() {
		boolean result = PasswordValidator.hasValidDigits("");
		assertFalse("Invalid Password must contain at least 2 digits", result);
	}

	@Test
	public void testHasValidDigitsBoundryIn() {
		boolean result = PasswordValidator.hasValidDigits("J0natha0n1");
		assertTrue("Invalid Password must contain at least 2 digits", result);
	}

	@Test
	public void testHasValidDigitsBoundryOut() {
		boolean result = PasswordValidator.hasValidDigits("Test 7");
		assertFalse("Invalid Password must contain at least 2 digits", result);
	}

	@Test
	public void testHasValidCaseCharacters() {
		boolean result = PasswordValidator.hasValidCaseCharacters("JonATHan");
		assertTrue("Invalid password must contain uppercase & lowercase letters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters("");
		assertFalse( "Invalid case characters" , result );		
	}	
	
	@Test
	public void testHasValidCaseCharsExceptionNull( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters(null);
		assertFalse( "Invalid case characters" , result );		
	}	
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters("123456789");
		assertFalse( "Invalid case characters" , result );		
	}		
	
	@Test
	public void testHasValidCaseCharsBoundaryIn( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters( "Jo" );
		assertTrue( "Invalid case characters" , result );		
	}	
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters( "JONA" );
		assertFalse( "Invalid case characters" , result );		
	}		

	@Test
	public void testHasValidCaseCharsBoundaryOutLower( ) {
		boolean result = PasswordValidator.hasValidCaseCharacters( "jona" );
		assertFalse( "Invalid case characters" , result );		
	}	

}
